<?php

namespace Krixon\JWT;

/**
 * Enumeration of supported headers.
 */
final class Header
{
    const TYP = 'typ'; // Object type. Used to disambiguate objects. Value should be JWT for JWTs.
    const CTY = 'cty'; // Content type. Used to convey structural information about the JWT.
    const ALG = 'alg'; // Algorithm. Conveys the algorithm used to produce the encrypted key.
    const ENC = 'enc'; // Encryption.
    
    const ENUM = [
        self::TYP,
        self::CTY,
        self::ALG,
        self::ENC,
    ];
}
