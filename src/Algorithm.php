<?php

namespace Krixon\JWT;

/**
 * Enumeration of supported algorithms.
 */
final class Algorithm
{
    const HS256 = 'HS256';
    const HS512 = 'HS512';
    const HS384 = 'HS384';
    const RS256 = 'RS256';
    
    const ALGORITHMS = [
        self::HS256 => ['hash_hmac', 'SHA256'],
        self::HS512 => ['hash_hmac', 'SHA512'],
        self::HS384 => ['hash_hmac', 'SHA384'],
        self::RS256 => ['openssl',   'SHA256'],
    ];
}
