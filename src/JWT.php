<?php

namespace Krixon\JWT;

use Krixon\DateTime\DateTime;

/**
 * A JSON web token.
 */
class JWT
{
    /**
     * @var string[]
     */
    private $headers;
    
    /**
     * @var array
     */
    private $payload;
    
    /**
     * @var string
     */
    private $signature;
    
    /**
     * @var DateTime|null
     */
    private $expiresAt;
    
    
    /**
     * @param array  $headers
     * @param array  $payload
     * @param string $signature
     */
    public function __construct(array $headers, array $payload, string $signature)
    {
        $this->headers   = $headers;
        $this->payload   = $payload;
        $this->signature = $signature;
        
        if (isset($data[Claim::EXP])) {
            $this->expiresAt = DateTime::fromTimestamp($data[Claim::EXP]);
        }
    }
    
    
    /**
     * Returns the token's headers as an associative array.
     *
     * @return string[]
     */
    public function headers()
    {
        return $this->headers;
    }
    
    
    /**
     * Returns the token's signature.
     *
     * @return string
     */
    public function signature() : string
    {
        return $this->signature;
    }
    
    
    /**
     * Returns the token payload.
     *
     * This is an array with arbitrary structure and contents.
     *
     * @return array
     */
    public function payload() : array
    {
        return $this->payload;
    }
    
    
    /**
     * Determines if this token makes a claim.
     *
     * @param string $claim
     *
     * @return bool
     */
    public function makesClaim(string $claim) : bool
    {
        return array_key_exists($claim, $this->payload);
    }
    
    
    /**
     * Returns a claim from the token, or $default if no such claim is made.
     *
     * @param string $claim
     * @param mixed  $default
     *
     * @return mixed
     */
    public function claim(string $claim, $default = null)
    {
        if ($this->makesClaim($claim)) {
            return $this->payload[$claim];
        }
        
        return $default;
    }
    
    
    /**
     * Determines if the token expires.
     *
     * @return bool
     */
    public function expires()
    {
        return $this->expiresAt() !== null;
    }
    
    
    /**
     * The date/time at which the token expires, or null if it does not expire.
     *
     * @return DateTime|null
     */
    public function expiresAt()
    {
        return $this->expiresAt;
    }
    
    
    /**
     * Determines if the token is expired.
     *
     * @return bool
     */
    public function isExpired() : bool
    {
        return $this->expires() && $this->expiresAt->isInThePast();
    }
}
