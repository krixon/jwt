<?php

namespace Krixon\JWT;

/**
 * Encodes and decodes JWTs.
 */
class Codec
{
    const CLOCK_SKEW = 0;
    
    /**
     * @var string
     */
    private $key;
    
    
    /**
     * @param string $key
     */
    public function __construct(string $key)
    {
        $this->key = $key;
    }
    
    
    /**
     * Encodes data into a JWT object.
     *
     * @param array  $payload
     * @param string $algorithm
     * @param array  $extraHeaders
     *
     * @return JWT
     */
    public function encode(array $payload, string $algorithm = 'HS512', array $extraHeaders = []) : JWT
    {
        $headers = [
            Header::TYP => 'JWT',
            Header::ALG => $algorithm
        ];
        
        $headers += $extraHeaders;
        
        $segments = [
            $this->base64UrlEncode($this->jsonEncode($headers)),
            $this->base64UrlEncode($this->jsonEncode($payload)),
        ];
        
        $signature  = $this->sign(implode('.', $segments), $algorithm);
        $segments[] = $this->base64UrlEncode($signature);
        $token      = implode('.', $segments);
        
        return new JWT($token, $headers, $payload, $signature);
    }
    
    
    /**
     * Decodes a token string into a JWT object.
     *
     * @param string $token
     *
     * @return JWT
     * @throws Exception\InvalidTokenException
     * @throws Exception\UnsupportedAlgorithmException
     */
    public function decode(string $token) : JWT
    {
        $segments = explode('.', $token);
        
        if (count($segments) !== 3) {
            throw new Exception\InvalidTokenException('Incorrect number of token segments.');
        }
        
        list ($encodedHeaders, $encodedPayload, $encodedSignature) = $segments;
        
        $headers   = $this->jsonDecode($this->base64UrlDecode($encodedHeaders));
        $payload   = $this->jsonDecode($this->base64UrlDecode($encodedPayload));
        $signature = $this->base64UrlDecode($encodedSignature);
        
        if (!isset($headers[Header::ALG])) {
            throw new Exception\InvalidTokenException('Missing algorithm header.');
        }
        
        if (!array_key_exists($headers[Header::ALG], Algorithm::ALGORITHMS)) {
            throw new Exception\InvalidTokenException('Unsupported algorithm.');
        }
        
        if (!$this->verify("$encodedHeaders.$encodedPayload", $signature, $headers['alg'])) {
            throw new Exception\InvalidTokenException('Signature verification failed.');
        }
        
        $now = time();
        
        // Validate "not before".
        if (isset($payload[Claim::NBF]) && $payload[Claim::NBF] > ($now + self::CLOCK_SKEW)) {
            throw new Exception\InvalidTokenException('Token is not valid yet.');
        }
        
        // Validate "issued at".
        if (isset($payload[Claim::IAT]) && $payload[Claim::IAT] > ($now + self::CLOCK_SKEW)) {
            throw new Exception\InvalidTokenException('Token is not valid yet.');
        }
        
        // Validate "expiration time".
        if (isset($payload[Claim::EXP]) && ($now - self::CLOCK_SKEW) >= $payload[Claim::EXP]) {
            throw new Exception\InvalidTokenException('Token is expired.');
        }
        
        return new JWT($token, $headers, $payload, $signature);
    }
    
    
    /**
     * Verifies a message based on its signature.
     *
     * @param string $message
     * @param string $signature
     * @param string $algorithm
     *
     * @return bool
     * @throws Exception\UnsupportedAlgorithmException
     */
    private function verify(string $message, string $signature, string $algorithm) : bool
    {
        list ($function, $algorithm) = Algorithm::ALGORITHMS[$algorithm];
        
        switch ($function) {
            case 'hash_hmac':
                return hash_equals($signature, hash_hmac($algorithm, $message, $this->key, true));
            case 'openssl':
                return 1 === openssl_verify($message, $signature, $this->key, $algorithm);
        }
        
        throw new Exception\UnsupportedAlgorithmException;
    }
    
    
    /**
     * @param string|$message
     * @param string|$algorithm
     *
     * @return string
     * @throws Exception\UnsupportedAlgorithmException
     */
    private function sign($message, $algorithm)
    {
        list ($function, $algorithm) = Algorithm::ALGORITHMS[$algorithm];
        
        switch ($function) {
            case 'hash_hmac':
                return hash_hmac($algorithm, $message, $this->key, true);
            case 'openssl':
                if (openssl_sign($message, $signature, $this->key, $algorithm)) {
                    return $signature;
                }
        }
        
        throw new Exception\UnsupportedAlgorithmException;
    }
    
    
    /**
     * @param mixed $data
     *
     * @return string
     */
    private static function jsonEncode($data) : string
    {
        return json_encode($data);
    }
    
    
    /**
     * @param string $json
     *
     * @return array
     */
    private static function jsonDecode(string $json) : array
    {
        return json_decode($json, true);
    }
    
    
    /**
     * @param string $string
     *
     * @return string
     */
    private static function base64UrlEncode(string $string) : string
    {
        return str_replace('=', '', strtr(base64_encode($string), '+/', '-_'));
    }
    
    
    /**
     * @param string $string
     *
     * @return string
     */
    private static function base64UrlDecode(string $string) : string
    {
        $remainder = strlen($string) % 4;
        
        if ($remainder) {
            $string .= str_repeat('=', 4 - $remainder);
        }
        
        return base64_decode(strtr($string, '-_', '+/'));
    }
}
