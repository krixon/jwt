<?php

namespace Krixon\JWT;

/**
 * Enumeration of supported claims.
 */
final class Claim
{
    const ISS = 'iss'; // Issuer of the JWT. Either a string or URI.
    const SUB = 'sub'; // Subject. Identifies the principle that is the subject of the JWT (eg, a user id).
    const AUD = 'aud'; // Audience. The intended recipients of the JWT (eg, a client id).
    const EXP = 'exp'; // Expiration time. The JWT must not be accepted on or after this. Unix timestamp.
    const NBF = 'nbf'; // Not before. The JWT must not be accepted before this. Unix timestamp.
    const IAT = 'iat'; // Issued at. The time at which the JWT was issued. Unix timestamp.
    const JTI = 'jti'; // JWT ID. A unique ID for the JWT. Can be used to prevent replay attacks.
    
    const ENUM = [
        self::ISS,
        self::SUB,
        self::AUD,
        self::EXP,
        self::NBF,
        self::IAT,
        self::JTI,
    ];
}
